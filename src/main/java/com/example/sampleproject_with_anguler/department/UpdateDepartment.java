package com.example.sampleproject_with_anguler.department;
import lombok.AllArgsConstructor;
import lombok.Value;
@Value
@AllArgsConstructor
public class UpdateDepartment {
    String departmentName;
    String color;
}
