package com.example.sampleproject_with_anguler.project;
import com.example.sampleproject_with_anguler.department.DepartmentEntity;
import com.example.sampleproject_with_anguler.department.DepartmentRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@AllArgsConstructor
@Service
public class ProjectService {
    @Autowired
    ProjectRepository projectRepository;
    DepartmentRepository departmentRepository;

    @Transactional
    public Optional<ProjectEntity> addNewProject(CreateProject createProject){
        String departmentName =  createProject.getDepartmentName();
        Optional<DepartmentEntity> departmentEntityOptional = departmentRepository.findByDepartmentName(departmentName)
                .findAny();

        ProjectEntity project = new ProjectEntity(
                UUID.randomUUID().toString(),
                createProject.getProjectName(),
               departmentEntityOptional.get()
        );
        projectRepository.save(project);
        return Optional.of(project);
    }
    public List<ProjectEntity> allProjects(){
        return (List<ProjectEntity>) projectRepository.findAll();
    }
    public Optional<ProjectEntity> findById(String id){
        return projectRepository.findById(id);
    }
}
