package com.example.sampleproject_with_anguler.department;
import com.example.sampleproject_with_anguler.project.ProjectEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "departments")
public class DepartmentEntity {
    @Id
    String id;
    String departmentName;
    String color;
    @OneToMany(mappedBy = "departmentEntity")
    List<ProjectEntity> projectEntities;

    public DepartmentEntity( String id,String departmentName, String color) {
        this.id= id;
        this.departmentName=departmentName;
        this.color=color;
        this.projectEntities= new ArrayList<>();
    }
}
