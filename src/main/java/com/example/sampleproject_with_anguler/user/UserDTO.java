package com.example.sampleproject_with_anguler.user;
import lombok.Builder;
import lombok.Value;

import java.util.List;
@Value
@Builder(toBuilder = true)
public class UserDTO {
    String id;
    String firstName;
    String lastName;
    String email;
    List<String> projects;

}
