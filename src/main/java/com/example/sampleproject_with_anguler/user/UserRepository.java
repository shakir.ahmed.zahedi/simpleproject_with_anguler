package com.example.sampleproject_with_anguler.user;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;
@Repository
public interface UserRepository extends CrudRepository<UserEntity,String> {
    Stream<UserEntity> findByFirstName(String firstName);
}
