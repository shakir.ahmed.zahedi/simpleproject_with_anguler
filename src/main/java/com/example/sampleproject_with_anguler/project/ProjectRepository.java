package com.example.sampleproject_with_anguler.project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface ProjectRepository extends CrudRepository<ProjectEntity,String> {
}
