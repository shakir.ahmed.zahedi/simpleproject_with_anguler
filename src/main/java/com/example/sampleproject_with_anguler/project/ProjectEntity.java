package com.example.sampleproject_with_anguler.project;
import com.example.sampleproject_with_anguler.department.DepartmentEntity;
import com.example.sampleproject_with_anguler.user.UserEntity;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "projects")
public class ProjectEntity {
    @Id
    String id;
    String projectName;
    @ManyToMany(mappedBy = "projects")
    List<UserEntity> users;
    @ManyToOne()
    @JoinColumn(name = "departmentEntity_id")
    DepartmentEntity departmentEntity;

    public ProjectEntity(String id, String projectName,DepartmentEntity departmentEntity) {
        this.id=id;
        this.projectName=projectName;
        this.departmentEntity=departmentEntity;
        this.users = new ArrayList<>();
    }
}
