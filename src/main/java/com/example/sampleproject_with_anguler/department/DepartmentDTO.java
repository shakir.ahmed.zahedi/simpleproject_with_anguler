package com.example.sampleproject_with_anguler.department;
import com.example.sampleproject_with_anguler.project.ProjectEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.util.List;
@Value
@Builder(toBuilder = true)
public class DepartmentDTO {
    String id;
    String departmentName;
    String color;
    List<String> projectEntityList;
}
