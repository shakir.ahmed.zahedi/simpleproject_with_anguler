package com.example.sampleproject_with_anguler.department;
import com.example.sampleproject_with_anguler.project.ProjectEntity;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
@RestController
@RequestMapping("/Department")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    @PostMapping("/addNew")
    public DepartmentDTO createNewDepartment(@RequestBody CreateDepartment createDepartment){
        return departmentService.createNewDepartment(createDepartment)
                .map(this::toDepartmentDTO)
                .orElse(null);
    }
    @GetMapping()
    public List<DepartmentDTO> allDepartments(){
        return departmentService.allDepartment().stream()
                .map(this::toDepartmentDTO)
                .collect(Collectors.toList());
    }
    @DeleteMapping("/{id}")
    public void deleteDomain(@PathVariable("id") String id){
        departmentService.deleteById(id);

    }
    @DeleteMapping("/{id}/{projectname}")
    public  void deleteProjectByName(@PathVariable("id")String id,
                                     @PathVariable("projectname")String projectName){
        departmentService.deleteProjectByName(id,projectName);
    }
    @PostMapping("/{id}")
    public DepartmentDTO updateDepartment(@PathVariable("id") String id,@RequestParam UpdateDepartment updateDepartment){
         DepartmentEntity department = departmentService.updateDepartment(id,updateDepartment)
                 .orElse(null);
         return toDepartmentDTO(department);
    }
    @GetMapping("/find")
    public List<DepartmentDTO> findByDepartmentName(@RequestParam("departmentName") String departmentName){
         return departmentService.findByName(departmentName).stream()
                .map(this::toDepartmentDTO)
                .collect(Collectors.toList());

    }

    private DepartmentDTO toDepartmentDTO(DepartmentEntity departmentEntity){
        return new DepartmentDTO(
                departmentEntity.getId(),
                departmentEntity.getDepartmentName(),
                departmentEntity.getColor(),
                departmentEntity.getProjectEntities().stream()
                .map(ProjectEntity::getProjectName)
                .collect(Collectors.toList())
        );
    }
}
