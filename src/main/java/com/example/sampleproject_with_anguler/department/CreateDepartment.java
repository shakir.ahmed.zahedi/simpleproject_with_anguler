package com.example.sampleproject_with_anguler.department;
import lombok.AllArgsConstructor;
import lombok.Value;
@Value
@AllArgsConstructor
public class CreateDepartment {
    String departmentName;
    String color;

}
