package com.example.sampleproject_with_anguler.project;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
@RestController
@RequestMapping("/project")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProjectController {
    @Autowired
    ProjectService projectService;

    @PostMapping("/newproject")
    public ProjectDTO createNewProject(@RequestBody CreateProject createProject){
        return projectService.addNewProject(createProject)
                .map(this::toProjectDTO)
                .orElse(null);
    }
    @GetMapping()
    public List<ProjectDTO>allProjects(){
        return projectService.allProjects().stream()
                .map(this::toProjectDTO)
                .collect(Collectors.toList());
    }

    private ProjectDTO toProjectDTO(ProjectEntity projectEntity){
         return new ProjectDTO(
                projectEntity.getId(),
                projectEntity.getProjectName(),
                 projectEntity.getDepartmentEntity().getDepartmentName()
        );
    }
}
