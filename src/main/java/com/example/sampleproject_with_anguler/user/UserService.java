package com.example.sampleproject_with_anguler.user;
import com.example.sampleproject_with_anguler.department.DepartmentController;
import com.example.sampleproject_with_anguler.department.DepartmentEntity;
import com.example.sampleproject_with_anguler.department.DepartmentRepository;
import com.example.sampleproject_with_anguler.project.ProjectEntity;
import com.example.sampleproject_with_anguler.projecttouser.AddProjectToUserDTO;
import com.example.sampleproject_with_anguler.projecttouser.RemoveProjectFromUserDTO;
import com.example.sampleproject_with_anguler.exception.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
@Service
@AllArgsConstructor
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    DepartmentRepository departmentRepository;

    public Optional<UserEntity> addNewUser(CreateUser createUser) {
        UserEntity user = new UserEntity(
                UUID.randomUUID().toString(),
             createUser.getFirstName(),
             createUser.getLastName(),
             createUser.getPassword(),
             createUser.getEmail()
            );

        userRepository.save(user);
        return Optional.of(user);
    }

    public List<UserEntity> getAll(){
        return (List<UserEntity>) userRepository.findAll();
    }

    public Optional<UserEntity> findById(String id){
        return userRepository.findById(id);
    }

    @Transactional
    public void deleteById(String id) {
        userRepository.deleteById(id);
    }
    @Transactional
    public List<UserEntity> findByName(String firstName) {
        return userRepository.findByFirstName(firstName)
                .collect(Collectors.toList());

    }
    @Transactional
    public UserEntity updateUser(String id, UpdateUser updateUser) {
        UserEntity user = userRepository.findById(id).orElse(null);
                user.setFirstName(updateUser.getFirstName());
                user.setLastName(updateUser.getLastName());
                user.setEmail(updateUser.getEmail());
              userRepository.save(user);
              return user;
    }
    @Transactional
    public Optional<UserEntity> addProjectToUser(AddProjectToUserDTO addProjectToUserDTO) throws NotFoundException {
        return Optional.of(applyToUser(
                addProjectToUserDTO.getFirstName(),
                addProjectToUserDTO.getDepartmentName(),
                addProjectToUserDTO.getProjectName(),
                UserEntity::addProject
        )
                .map(this::save)
                .get());
    }
    @Transactional
    public UserEntity removeProjectFromUser(RemoveProjectFromUserDTO removeProjectFromUserDTO) throws NotFoundException {
        return applyToUser(
                removeProjectFromUserDTO.getFirstName(),
                removeProjectFromUserDTO.getDepartmentName(),
                removeProjectFromUserDTO.getProjectName(),
                UserEntity::deleteProject
                )
                .map(this::save)
                .get();
    }

    @FunctionalInterface
    interface UserProject {
        void apply(UserEntity user, ProjectEntity project);
    }

    private UserEntity save(UserEntity user) {
        return userRepository.save(user);
    }

    public Optional<UserEntity> applyToUser(String firstName,String departmentName,String projectName,
                                             UserProject userProject) throws NotFoundException{
        Optional<UserEntity> optionalUser = userRepository.findByFirstName(firstName).findAny();
        if (optionalUser.isEmpty())
            throw new NotFoundException("User with name '" + firstName + "' not found!");
        System.out.println("user:"+optionalUser);
       // System.out.println("-------"+departmentRepository.findAll());

        //DepartmentEntity department =departmentRepository.findById(id).get();
        Optional<DepartmentEntity> optionalDepartment = departmentRepository.findByDepartmentName(departmentName).findAny();

        System.out.println("dep:"+optionalDepartment);
        if (optionalDepartment.isEmpty())
            throw new NotFoundException("User with name '" + departmentName + "' not found!");
        DepartmentEntity department = optionalDepartment.get();
        Optional<ProjectEntity> optionalProject = department.getProjectEntities().stream()
                .filter(projectEntity -> projectEntity.getProjectName().equalsIgnoreCase(projectName))
                .findAny();
        ProjectEntity project = optionalProject.get();
        UserEntity user = optionalUser.get();
        userProject.apply(user,project);
        return Optional.of(user);
    }


}
