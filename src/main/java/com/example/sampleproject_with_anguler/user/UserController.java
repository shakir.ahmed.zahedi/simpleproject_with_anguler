package com.example.sampleproject_with_anguler.user;
import com.example.sampleproject_with_anguler.projecttouser.AddProjectToUserDTO;
import com.example.sampleproject_with_anguler.projecttouser.RemoveProjectFromUserDTO;
import com.example.sampleproject_with_anguler.exception.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
@RestController
@RequestMapping("/user")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping("/addUser")
    public UserDTO addNewUser(@RequestBody CreateUser createUser){
        UserDTO userDTO = userService.addNewUser(createUser)
                .map(this::toUserDTO)
                .orElse(null);
        return  userDTO;
    }
    @PostMapping("/{id}")
    public UserDTO updateUser(@RequestBody UpdateUser updateUser,@PathVariable("id") String id){
        UserEntity user = userService.updateUser(id,updateUser);
        return  toUserDTO(user);
    }
    @GetMapping()
    public List<UserDTO> all(){
        return userService.getAll().stream()
                .map(this::toUserDTO)
                .collect(Collectors.toList());
    }
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") String id){
        userService.deleteById(id);
    }
    @GetMapping("/{id}")
    public void findById(@PathVariable("id") String id){
        userService.findById(id);
    }
    @GetMapping("/find")
    public List<UserDTO> findByFirstName(@RequestParam("firstName") String firstName){
        return userService.findByName(firstName).stream()
                .map(this::toUserDTO)
                .collect(Collectors.toList());

    }
    @PostMapping("/addProjectToUser")
    public UserDTO addProjectToUser(@RequestBody AddProjectToUserDTO addProjectToUserDTO) throws NotFoundException {
        UserEntity user = userService.addProjectToUser(addProjectToUserDTO).orElse(null);
               return toUserDTO(user);
    }
    @DeleteMapping("/removeProjectFromUser")
    public UserDTO removeProjectFromUser(@RequestBody RemoveProjectFromUserDTO removeProjectFromUserDTO) throws NotFoundException{
        UserEntity user = userService.removeProjectFromUser(removeProjectFromUserDTO);
        return toUserDTO(user);
    }


    private UserDTO toUserDTO(UserEntity userEntity) {
        return new UserDTO(
                userEntity.getId(),
                userEntity.getFirstName(),
                userEntity.getLastName(),
                userEntity.getEmail(),
                userEntity.getProjects().stream()
                .map(projectEntity -> projectEntity.getDepartmentEntity().getDepartmentName()
                        +"DEPARTMENT HAS"+ projectEntity.getProjectName())
                .collect(Collectors.toList())
        );
    }
}
