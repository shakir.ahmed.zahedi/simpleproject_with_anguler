package com.example.sampleproject_with_anguler.project;
import lombok.AllArgsConstructor;
import lombok.Value;
@Value
@AllArgsConstructor
public class CreateProject {
    String projectName;
    String departmentName;
}
