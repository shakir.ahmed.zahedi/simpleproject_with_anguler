package com.example.sampleproject_with_anguler.project;
import lombok.Builder;
import lombok.Value;
@Value
@Builder(toBuilder = true)
public class ProjectDTO {
    String id;
    String projectName;
    String departmentName;
}
