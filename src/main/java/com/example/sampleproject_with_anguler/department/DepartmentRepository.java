package com.example.sampleproject_with_anguler.department;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;
@Repository
public interface DepartmentRepository extends CrudRepository<DepartmentEntity,String> {
    Stream<DepartmentEntity> findByDepartmentName(String departmentName);
}
