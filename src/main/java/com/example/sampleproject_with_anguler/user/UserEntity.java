package com.example.sampleproject_with_anguler.user;
import com.example.sampleproject_with_anguler.project.ProjectEntity;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
public class UserEntity {
    @Id
    String id;
    @Column(name = "firstname")
    String firstName;
    @Column(name = "lastname")
    String lastName;
    @Column(name = "password")
    String password;
    @Column(name = "email")
    String email;
    @ManyToMany()
    @JoinTable(name = "userEntity_projectEntity",
            joinColumns = @JoinColumn(name = "userEntity_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "projectEntity_id", referencedColumnName = "id"))

    List<ProjectEntity> projects;

    public UserEntity(String id, String firstName, String lastName, String password, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.projects = new ArrayList<>();
    }
    public void addProject( ProjectEntity project){
        this.projects.add(project);
    }
    public void deleteProject( ProjectEntity project){
        this.projects.remove(project);
    }
}
