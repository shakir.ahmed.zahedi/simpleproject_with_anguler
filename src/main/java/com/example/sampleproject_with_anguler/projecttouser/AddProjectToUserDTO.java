package com.example.sampleproject_with_anguler.projecttouser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
@Value
@Builder(toBuilder = true)
public class AddProjectToUserDTO {
    String firstName;
    String departmentName;
    String projectName;
}
