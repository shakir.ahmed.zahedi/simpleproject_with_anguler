package com.example.sampleproject_with_anguler.department;
import com.example.sampleproject_with_anguler.project.ProjectEntity;
import com.example.sampleproject_with_anguler.project.ProjectRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
@AllArgsConstructor
@NoArgsConstructor
@Service
public class DepartmentService {
    @Autowired
    DepartmentRepository departmentRepository;
    ProjectRepository projectRepository;

    public Optional<DepartmentEntity> createNewDepartment(CreateDepartment createDepartment){
        DepartmentEntity department = new DepartmentEntity(
                UUID.randomUUID().toString(),
                createDepartment.getDepartmentName(),
                createDepartment.getColor()
        );
        departmentRepository.save(department);
        return Optional.of(department);
    }

    public void deleteById( String id){
        departmentRepository.deleteById(id);
    }

    public List<DepartmentEntity> allDepartment(){
        return (List<DepartmentEntity>) departmentRepository.findAll();
    }

    public Optional<DepartmentEntity> findById(String id){
        return departmentRepository.findById(id);
    }

    @Transactional
    public void deleteProjectByName(String id, String projectName) {
        DepartmentEntity department = departmentRepository.findById(id)
                .orElse(null);
        ProjectEntity project = department.getProjectEntities().stream()
                .filter(r-> r.getProjectName().equalsIgnoreCase(projectName))
                .findFirst()
                .orElse(null);
        department.getProjectEntities().remove(projectName);
        projectRepository.delete(project);
        departmentRepository.save(department);
    }

    public Optional<DepartmentEntity> updateDepartment( String id,UpdateDepartment updateDepartment) {
        DepartmentEntity department = departmentRepository.findById(id).orElse(null);
        department.setDepartmentName(updateDepartment.getDepartmentName());
        department.setColor(updateDepartment.getColor());
         departmentRepository.save(department);
         return Optional.of(department);

    }

    @Transactional
    public List<DepartmentEntity> findByName(String departmentName) {
        return departmentRepository.findByDepartmentName(departmentName)
                .collect(Collectors.toList());
    }
}
