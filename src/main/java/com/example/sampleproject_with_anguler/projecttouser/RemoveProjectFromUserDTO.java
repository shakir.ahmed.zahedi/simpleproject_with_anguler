package com.example.sampleproject_with_anguler.projecttouser;
import lombok.Value;
@Value
public class RemoveProjectFromUserDTO {
    String firstName;
    String departmentName;
    String projectName;
}
