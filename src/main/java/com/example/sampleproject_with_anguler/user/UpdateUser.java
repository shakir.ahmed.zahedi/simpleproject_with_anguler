package com.example.sampleproject_with_anguler.user;
import lombok.AllArgsConstructor;
import lombok.Value;
@Value
@AllArgsConstructor
public class UpdateUser {
    String firstName;
    String lastName;
    String password;
    String email;
}
