package com.example.sampleproject_with_anguler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class SampleprojectWithAngulerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleprojectWithAngulerApplication.class, args);
    }
}
